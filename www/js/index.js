"use strict";

var app = {
	apiToken: null,
	apiAuth: 0,
	apiURL: "https://api.t-vend.co.uk/public/",
	db: null,
	sectionCurrent: null,
	sectionInit: [],
	tables: ["machine_category", "machine", "sku_category", "sku", "restock", "stock"],
	skuCategories: [],
	machineCategories: [],
	filters: {
		skuFilterIndex: 0,
		machineFilterIndex: 0
	},
	lastMachine: null,
	inEdition: "",
	currentMachineName: "",
	lastSKU: null,
	lastScan: null,
	syncPoint: 0,
	settings: {
		test_enable: 0,
		test_time: "10:00",
		test_words: ["", "", "", "", ""],
	},

	quit: function (i) {
		if (i != null) {
			if (app.db != null) {
				app.db.close (function () {
					app.db = null;
				}, app.dbNull);

			} else {
				if (navigator && navigator.app)
					navigator.app.exitApp ();
			}
		}

		setTimeout (function () { app.quit (true); }, 100);
	},

	initialise: function () {
		//document.addEventListener ("load", function () {}, false);
		//document.addEventListener ("pause", function () {}, false);
		//document.addEventListener ("resume", function () {}, false);
		//document.addEventListener ("online", function () {}, false);
		//document.addEventListener ("offline", function () {}, false);

		document.addEventListener ("deviceready", this.onDeviceReady, false);
	},

	onDeviceReady: function () {
		/*cordova.plugins.notification.local.on ("click", function (notification, state) {
			app.sectionCurrent = "test";
		}, this);*/

		document.addEventListener ("backbutton", function () {
			if (app.sectionCurrent == null || app.sectionCurrent == "home") {
				app.quit ();
			} else {
				app.sectionShow ("home");
			}
		}, false);

		app.dbInit ();
		setTimeout (app.uiNavAttach, 25);
		setTimeout (app.dataLoad, 50);
		setTimeout (app.uiStart, 100);
		//app.externalLinkHijack ();
	},

	dbNull: function (sqlTemp) {
	},

	dbInit: function () {
		if (cordova.platformId != "browser") {
			app.db = window.sqlitePlugin.openDatabase ({name: "tvendcache", location: 2}, app.dbCheck, app.dbCorrupt);
		} else {
			app.db = window.openDatabase ("tvendcache", "1.0", "T-Vend Local Cache", 1024 * 1024);
			app.db.executeSql = function (sqlOperation, sqlFields, sqlCallbackSuccess, sqlCallbackFail) {
				app.db.transaction (function (sqlTransaction) {
					sqlTransaction.executeSql (sqlOperation, sqlFields, function (sqlTransaction, sqlResult) { sqlCallbackSuccess (sqlResult); }, sqlCallbackFail);
				});
			};

			if (app.db == null) {
				app.dbCorrupt ();
			} else {
				app.dbCheck (app.db);
			}
		}

		app.db.quickSql = function (sqlOperation) {
			app.db.executeSql (sqlOperation, [], app.dbNull, app.dbNull);
		};
	},

	dbCheck: function (sqlDb) {
		app.db.executeSql ("SELECT 1 FROM flags", [], app.dbNull, function (sqlError) {
			app.db.transaction (function (sqlTransaction) {
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS flags (version_code TEXT, api_token TEXT, api_auth INTEGER, sync_point INTEGER)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS machine (machine_id TEXT PRIMARY KEY, sync_point INTEGER, machine_deleted TINYINT, machine_barcode TEXT, machine_category_id TEXT, machine_name TEXT, machine_client TEXT, machine_location TEXT, machine_notes TEXT)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sync_point ON machine (sync_point)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS machine_deleted ON machine (machine_deleted)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS machine_barcode ON machine (machine_barcode)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS machine_category (machine_category_id TEXT PRIMARY KEY, sync_point INTEGER, machine_category_deleted TINYINT, machine_category_name TEXT)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sync_point ON machine_category (sync_point)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS machine_category_deleted ON machine_category (machine_category_deleted)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS sku (sku_id TEXT PRIMARY KEY, sync_point INTEGER, sku_deleted TINYINT, sku_barcode TEXT, sku_category_id INTEGER, sku_name TEXT, sku_notes TEXT)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sync_point ON sku (sync_point)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sku_deleted ON sku (sku_deleted)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sku_barcode ON sku (sku_barcode)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sku_category_id ON sku (sku_category_id)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS sku_category (sku_category_id INTEGER AUTO_INCREMENT PRIMARY KEY, sync_point INTEGER, sku_category_deleted TINYINT, sku_category_name TEXT)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sync_point ON sku_category (sync_point)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sku_category_deleted ON sku_category (sku_category_deleted)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS stock (sync_point INTEGER, machine_id TEXT, sku_id TEXT, stock_quantity INTEGER, stock_star TINYINT)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sync_point ON stock (sync_point)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS machine_id ON stock (machine_id)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS sku_id ON stock (sku_id)");
				sqlTransaction.executeSql ("CREATE TABLE IF NOT EXISTS restock (machine_id TEXT, sku_id TEXT, stock_sold INTEGER, stock_removed INTEGER, stock_added INTEGER)");
				sqlTransaction.executeSql ("CREATE INDEX IF NOT EXISTS machine_id ON restock (machine_id)");
				sqlTransaction.executeSql ("DELETE FROM flags");
				sqlTransaction.executeSql ("INSERT INTO flags (version_code, api_token, api_auth, sync_point) VALUES (\"1.0\", \"" + app.genUUID () + "\", 0, 0)");
			});
		});
	},

	dbCorrupt: function (sqlError) {
		navigator.notification.alert ("There is a little database problem, the app data needs to be wiped", null, "Oh dear", "Ok");
		app.quit ();
	},

	dbWipe: function () {
		app.db.quickSql ("DROP TABLE flags");
		app.db.quickSql ("DROP TABLE machine");
		app.db.quickSql ("DROP TABLE machine_category");
		app.db.quickSql ("DROP TABLE sku");
		app.db.quickSql ("DROP TABLE sku_category");
		app.db.quickSql ("DROP TABLE stock");
		app.db.quickSql ("DROP TABLE restock");
	},

	dbError: function () {
		app.toastShow ("Database grumbled, go back and try again");
	},

	ajax: function (requestAPI, requestVersion, requestMethod, requestData, requestCallback) {
		$.support.cors = true;
		$.ajax ({
			url: app.apiURL + encodeURIComponent (requestAPI) + "/" + encodeURIComponent (requestVersion) + "/" + encodeURIComponent (requestMethod),
			method: "POST",
			headers: {"Authorization": "Bearer " + "4300345c-736d-4ce4-a880-fc3c5e92be98"/*app.apiToken*/},
			timeout: 15000,
			crossDomain: true,
			scriptCharset: "UTF-8",
			data: requestData,
			dataType: "json",
			success: requestCallback,
			error: requestCallback,
		});
	},

	settingLoad: function (settingName, sqlResult) {
		if (sqlResult.rows.item (0)[settingName] != null)
			app.settings[settingName] = sqlResult.rows.item (0)[settingName];
	},

	settingSave: function (settingName, settingValue) {
		app.settings[settingName] = settingValue;
		if (typeof settingValue == "string")
			settingValue = "\"" + settingValue + "\"";
		app.db.quickSql ("UPDATE flags SET " + settingName + " = " + settingValue);
	},

	genUUID: function () {
		var now = new Date ().getTime ();
		var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace (/[xy]/g, function (char) {
			var rand = (now + Math.random () * 16) % 16 | 0;
			now = Math.floor (now / 16);
			return (char == "x" ? rand : (rand & 0x3 | 0x8)).toString (16);
		});

		return uuid;
	},

	getTime: function () {
		return Math.floor (Date.now () / 1000);
	},

	uiNavAttach: function () {
		new Hammer ($(".mdl-layout__drawer")[0]).on ("panstart panleft panright panend panup pandown", function (ev) {
			if (ev.type == "panend") {
				if (ev.deltaX < -30) {// || ev.deltaX > 30) {
					$(".mdl-layout__drawer-button i").click ();
				}
			}
		});

		for (var i = 0; i < 2; ++i) {
			$(i == 0 ? "nav a" : "header ul li.mdl-menu__item").each (function () {
				var _this = $(this);

				if (_this[0].hasAttribute ("id")) {
					_this.drawer = !i;

					$(this).on ("click", function () {
						if (_this.drawer)
							$(".mdl-layout__drawer-button i").click ();

						setTimeout (function () {
							app.sectionShow (_this.attr ("id").substring (3));
						}, _this.drawer ? 100 : 1);
					});
				}
			});
		}

		$("#quickscan").on ("click", function () {
			app.quickScan ();
		});
	},

	dataLoad: function () {
		app.machineCategories = [];
		app.db.executeSql ("SELECT machine_category_id, sync_point, machine_category_name FROM machine_category WHERE machine_category_deleted = 0 ORDER BY machine_category_name ASC", [], function (res) {
			for (var i = 0; i < res.rows.length; ++i)
				app.machineCategories.push (res.rows.item (i));
		}, app.dbNull);

		app.skuCategories = [];
		app.db.executeSql ("SELECT sku_category_id, sync_point, sku_category_name FROM sku_category WHERE sku_category_deleted = 0 ORDER BY sku_category_name ASC", [], function (res) {
			for (var i = 0; i < res.rows.length; ++i)
				app.skuCategories.push (res.rows.item (i));
		}, app.dbNull);
	},

	uiStart: function () {
		app.db.executeSql ("SELECT api_token, api_auth, sync_point, \"other settings\" FROM flags", [], function (sqlResult) {
			app.apiToken = sqlResult.rows.item (0).api_token;
			app.apiAuth = sqlResult.rows.item (0).api_auth;
			app.syncPoint = sqlResult.rows.item (0).sync_point;
			//app.settingLoad ("other settings", sqlResult); // TODO

			if (app.apiAuth == 1 || !app.sectionShow ("auth")) {
				app.uiShow (true);
				app.sectionShow (app.sectionCurrent == null ? "home" : app.sectionCurrent);
			}
		}, app.dbNull);
	},

	uiShow: function (isVisible) {
		if (isVisible) {
			$("#mdl-ui").removeClass ("hide");
		} else {
			$("#mdl-ui").addClass ("hide");
		}
	},

	toastShow: function (toastText) {
		var notification = $("#snackbar");
		notification[0].MaterialSnackbar.showSnackbar ({message: toastText});
	},

	sectionShow: function (sectionName, sectionAction) {
		if (sectionAction != "edit" || sectionAction != "add")
			app.inEdition = "";

		var sectionID = "#section" + sectionName;
		var sectionInit = (app.sectionInit.indexOf (sectionName) > -1 ? false : true);
		if (sectionInit)
			app.sectionInit.push (sectionName);

		if (app.sectionCurrent != null) {
			$("#section" + app.sectionCurrent).addClass ("hide");
			$(window).scrollTop (0);
		}
		app.sectionCurrent = sectionName;

		$(function () {
			$(sectionID + " article").each (function (i) {
				$(this).css ({"opacity": 0});
				$(this).delay (500 + (++i * 250)).fadeTo (500, 1);
			})
		});

		$(sectionID).removeClass ("hide");

		sectionName += "Section";
		return (app[sectionName] ? app[sectionName] (sectionInit, sectionAction) : true);
	},

	quickScan: function () {
		cordova.plugins.barcodeScanner.scan (
			function (result) {
				if (result.cancelled) {
					app.toastShow ("Barcode scanning cancelled");
				} else {
					if (app.inEdition != "") {
						$("#formskueditbarcode").val (result.text).focus ().parent ().addClass ("is-valid is-dirty").removeClass ("is-invalid");
						$("#formmachineeditbarcode").val (result.text).focus ().parent ().addClass ("is-valid is-dirty").removeClass ("is-invalid");
					} else {
						if (result.text.length == 6 && result.text.substring (0, 3) == "441") {
							app.db.executeSql ("SELECT machine_id, machine_name FROM machine WHERE machine_barcode = \"" + result.text + "\"", [], function (sqlResult) {
								if (sqlResult.rows.length == 1) {
									app.toastShow ("Now restocking " + sqlResult.rows.item (0).machine_name);
									app.lastMachine = sqlResult.rows.item (0).machine_id;
									app.currentMachineName = sqlResult.rows.item (0).machine_name;
									app.sectionShow ("restock", "do");
								} else {
									navigator.notification.beep ();
									navigator.notification.confirm ("This machine barcode is new, add it?", function (buttonIndex) {
										if (buttonIndex == 1) {
											app.lastMachine = null;
											app.lastScan = result.text;
											app.inEdition = "machine";
											app.sectionShow ("machinelist", "add");
										} else {
											if (app.sectionCurrent == "restock")
												app.toastShow ("Continuing on last machine");
										}
									}, "New machine", ["Add", "Cancel"]);
								}
							}, app.dbError);
						} else {
							app.db.executeSql ("SELECT sku_id FROM sku WHERE sku_barcode = \"" + result.text + "\"", [], function (sqlResult) {
								if (sqlResult.rows.length == 1) {
									if (app.lastMachine != null) {
										app.lastSKU = sqlResult.rows.item (0).sku_id;
										app.sectionShow ("restock", "edit");
									} else {
										app.toastShow ("Scan a machine first to start restocking");
									}
								} else {
									app.toastShow ("SKU " + result.text + " (" + result.format + ") is new, adding…");
									app.lastScan = result.text;
									app.sectionShow ("skulist", "add");
								}
							}, app.dbError);
						}
					}
				}
			},
			function (error) {
				app.toastShow ("Scanning error: " + error);
			}
		);
	},

	authSection: function (sectionInit) {
		var sectionAuth = $("#sectionauth");

		if (sectionInit) {
			var authChecker = function () {
				app.ajax ("test", "1", "auth.check", null, authCallback);
			};

			var authCallback = function (xhr, description) {
				if (!xhr.success) {
					setTimeout (authChecker, 5000);
				} else {
					app.db.quickSql ("UPDATE flags SET api_auth = 1");
					sectionAuth.empty ();
					app.uiShow (true);
					app.sectionShow ("home");
				}
			};
		}

		authChecker ();

		return true;
	},

	settingsSection: function (sectionInit) {
		if (sectionInit) {
			$("#settingtestenable").on ("click", function () {
				if ($(this).prop ("checked")) {
					$("#settingtest tr").each (function (i) {
						$(this).removeClass ("disabled");
						$("input", this).prop ("disabled", false);
					});
				} else {
					$("#settingtest tr").each (function (i) {
						if (i > 0) {
							$(this).addClass ("disabled");
							$("input", this).prop ("disabled", true);
						}
					});
				}
			});
			$("#settingtestenable").on ("change", function () {
				app.settingSave ("test_enable", $(this).prop ("checked") ? 1 : 0);
				app.notificationControl ();
			});

			$("#settingtesttime").val (app.settings.test_time);
			$("#settingtesttime").on ("change", function () { app.settingSave ("test_time", $("#settingtesttime").val ()); app.notificationControl (); });

			$("#settingtestspeed")[0].MaterialSlider.change (app.settings.test_speed);
			$("#settingtestspeed").on ("change", function () { app.settingSave ("test_speed", parseInt ($("#settingtestspeed").val ())); });

			$("#settingtestreps-" + app.settings.test_reps).click ();
			$("#settingtestreps-3").on ("click", function () { app.settingSave ("test_reps", 3); });
			$("#settingtestreps-5").on ("click", function () { app.settingSave ("test_reps", 5); })

			$("#settingtestvibrate").prop ("checked", !app.settings.test_vibrate);
			$("#settingtestvibrate").click ();
			$("#settingtestvibrate").on ("change", function () { app.settingSave ("test_vibrate", $(this).prop ("checked") ? 1 : 0); });
		}

		$("#settingtestenable").prop ("checked", !app.settings.test_enable);
		$("#settingtestenable").click ();
	},

	helpSection: function (sectionInit) {
		var cardHelpWipeRecreateDo = function (dbCheck) {
			app.dbWipe ();

			if (dbCheck) {
				app.dbCheck ();
				navigator.notification.alert ("Database wiped and recreated", null, "Done", "Ok");
			} else {
				navigator.notification.alert ("Database wiped, closing app", null, "Done", "Ok");
				app.quit ();
			}
		};

		var cardHelpWipeLinkDo = function (i) {
			if (i == 1)
				cardHelpWipeRecreateDo (false);
		};

		var cardHelpRecreateLinkDo = function (i) {
			if (i == 1)
				cardHelpWipeRecreateDo (true);
		};

		if (sectionInit) {
			$("#cardhelphomebtn").on ("click", function () {
				app.sectionShow ("home");
			});

			$("#cardhelpwipebtn").on ("click", function () {
				navigator.notification.confirm ("Really wipe the database?", cardHelpWipeLinkDo, "Confirm", ["Yes", "No"]);
				return false;
			});

			$("#cardhelprecreatebtn").on ("click", function () {
				navigator.notification.confirm ("Really wipe and recreate the database?", cardHelpRecreateLinkDo, "Confirm", ["Yes", "No"]);
				return false;
			});

			cordova.getAppVersion.getVersionNumber (function (versionNumber) {
				$("#cardhelp strong").text ("Version " + versionNumber);
			});

			new Hammer ($("#cardhelp strong")[0]).on ("press", function (ev) {
				$("#cardhelp span.hide").removeClass ("hide");
			});
		}
	},

	stockSection: function () {
		app.sectionShow ("skulist");
	},

	homeSection: function (sectionInit) {
		if (sectionInit) {
			$("#cardrestockbtn").on ("click", function () {
				if (app.currentMachineName != "") {
					app.sectionShow ("restock", "do");
				} else {
					app.sectionShow ("restock");
				}
			});

			$("#cardquerybtn").on ("click", function () {
				//app.sectionShow ("restock"); // FIXME huh?
				app.sectionShow ("settings");
			});
			$("#homemachinelistbtn").on ("click", function () {
				app.sectionShow ("machinelist");
			});
		}
	},

	restockSection: function (sectionInit, sectionAction) {
		var recordStock = {
			stock_qty: 0
		};

		var recordRestock = {
			stock_sold: 0,
			stock_removed: 0,
			stock_added: 0
		};

		if (sectionInit) {
			var doStockCalc = function (bias) {
				var storeBias = $("#cardrestockeditlast").data ("bias");
				if (bias != null && storeBias == "") {
					$("#cardrestockeditlast").data ("bias", bias);
					storeBias = bias;
				}

				if (bias == "sold" && storeBias == "sold") {
					$("#cardrestockeditadded").val ($("#cardrestockeditsold").val ());
				} else if (bias == "added" && storeBias == "added") {
					$("#cardrestockeditsold").val ($("#cardrestockeditadded").val ());
				}

				var stockNow = parseInt ($("#cardrestockeditlast").text ()) - parseInt ($("#cardrestockeditsold").val ()) - parseInt ($("#cardrestockeditremoved").val ()) + parseInt ($("#cardrestockeditadded").val ());

				$("#cardrestockeditnow").text (stockNow);
				if (stockNow > -1) {
					$("#cardrestockeditbtn").removeProp ("disabled");
				} else {
					$("#cardrestockeditbtn").prop ("disabled", "true");
				}
			};

			$("#finish_restock").on ("click", function (e) {
				e.preventDefault ();
				app.sectionShow ("sync");
			});

			$("#cardrestockeditbtn").on ("click", function () {
				app.db.quickSql ("INSERT INTO restock (machine_id, sku_id, stock_sold, stock_added, stock_removed) VALUES (\"" + app.lastMachine + "\", \"" + app.lastSKU + "\", " + $("#cardrestockeditsold").val () + ", " + $("#cardrestockeditremoved").val () + ", " + $("#cardrestockeditadded").val () + ")");
				app.sectionShow ("restock","do");
				return false;
			});

			$("#cardrestockcancelbtn").on ("click", function () {
				app.sectionShow ("restock","do");
				return false;
			});

			$("#restockmanual").on ("change", function () {
				var machine_id = $("#restockmanual").val ();
				if (machine_id > "") {
					$("#restockmanual")[0].selectedIndex = 0;
					app.db.executeSql ("SELECT machine_name FROM machine WHERE machine_id = \"" + machine_id + "\"", [], function (sqlResult) {
						app.currentMachineName = sqlResult.rows.item (0).machine_name;
						app.lastMachine = machine_id;
						app.sectionShow ("restock", app.lastSKU ? "edit" : "do");
					}, app.dbError);
				}
			});

			$("#cardquerydobtn2").on ("click", function () {
				app.lastMachine = null;
				app.lastSKU = null;
				app.sectionShow ("restock");
			});

			$("#stocklookupbtn").on ("click", function () {
				app.sectionShow ("skulist");
			});

			$("#cardrestockeditsold").on ("change", function () {
				doStockCalc ("sold");
			});

			$("#cardrestockeditadded").on ("change", function () {
				doStockCalc ("added");
			});

			$("#cardrestockeditremoved").on ("change", function () {
				doStockCalc (null);
			});
		}

		app.db.executeSql ("SELECT machine_id, machine_client, machine_name, machine_location FROM machine WHERE machine_deleted = 0 ORDER BY machine_client ASC, machine_location ASC, machine_name ASC", [], function (sqlResult) {
			$("#restockmanual").empty ();
			$("#restockmanual").append ("<option value=\"\">Select a machine manually&hellip;</option>\n");

			for (var i = 0; i < sqlResult.rows.length; ++i) {
				var record = sqlResult.rows.item (i);
				$("#restockmanual").append ("<option value=\"" + record.machine_id + "\">" + (record.machine_client.length > 0 ? record.machine_client + ": " : "") + record.machine_name + (record.machine_location.length > 0 ? " (" + record.machine_location + ")" : "") + "</option>\n");
			}
		}, app.dbNull);

		app.db.executeSql ("SELECT machine_name, machine_location FROM machine WHERE machine_id IN (SELECT DISTINCT machine_id FROM restock) ORDER BY machine_client ASC, machine_location ASC, machine_name ASC", [], function (sqlResult) {
			for (var i = 0; i < sqlResult.rows.length; ++i) {
				var record = sqlResult.rows.item (i);
				var restock = $("<li class=\"mdl-list__item mdl-list__item--two-line is-active\">\n<span class=\"mdl-list__item-primary-content\">\n<i class=\"material-icons mdl-list__item-avatar\">gradient</i>\n<span>" + record.machine_name + "</span>\n<span class=\"mdl-list__item-sub-title\"><span color=\"mdl-color__grey\">Location</span> " + record.machine_location + "</span>\n</span>\n</li>\n");

				$("#cardrestock ul").append (restock);
			}
		}, app.dbError);

		if (sectionAction == "do") {
			$("#cardrestockhome").addClass ("hide");
			$("#cardrestockedit").addClass ("hide");
			$("#cardrestockdo").removeClass ("hide");
			$("#restockmachinename").text (app.currentMachineName);

			app.db.executeSql ("SELECT sku.sku_name, restock._ROWID_, restock.stock_sold, restock.stock_removed, restock.stock_added FROM restock LEFT JOIN sku ON sku.sku_id = restock.sku_id WHERE restock.machine_id = \"" + app.lastMachine + "\" ORDER BY sku.sku_name ASC", [], function (sqlResult) {
				$("#cardrestockdo tbody").empty ();

				for (var i = 0; i < sqlResult.rows.length; ++i) {
					var record = sqlResult.rows.item (i);
					var restock = $("<tr>\n <td class=\"mdl-data-table__cell--non-numeric\" style=\"width:250px;\">" + record.sku_name + "</td>\n <td>" + record.stock_sold + "</td>\n <td>" + record.stock_removed + "</td>\n <td>" + record.stock_added + "</td>\n <td>x</td>\n </tr>\n");

					$("#cardrestockdo tbody").append (restock);
					var row = $("#cardrestockdo tbody tr")[i];
					row.rowid = record.rowid;
					new Hammer (row).on ("press", function (ev) {
						navigator.notification.confirm ("This cannot be undone. Are you sure?", function (btnIndex) {
							if (btnIndex == 1) {
								app.db.quickSql ("DELETE FROM restock WHERE _ROWID_ = " + $(ev.target).parent ()[0].rowid);
								app.sectionShow ("restock", "do");
							}
						},"Delete entry", ["Delete","Cancel"]);
					});
				}
			}, app.dbError);
		} else if (sectionAction == "edit") {
			$("#cardrestockdo").addClass ("hide");
			$("#cardrestockhome").addClass ("hide");
			$("#cardrestockedit").removeClass ("hide");

			app.db.executeSql ("SELECT stock_quantity FROM stock WHERE machine_id = \"" + app.lastMachine + "\" AND sku_id = \"" + app.lastSKU + "\"", [], function (sqlResult) {
				if (sqlResult.rows.length == 1)
					var recordStock = sqlResult.rows.item (0);
			}, app.dbError);

			app.db.executeSql ("SELECT stock_sold, stock_removed, stock_added FROM restock WHERE machine_id = \"" + app.lastMachine + "\" AND sku_id = \"" + app.lastSKU + "\"", [], function (sqlResult) {
				if (sqlResult.rows.length == 1)
					var recordRestock = sqlResult.rows.item (0);
			}, app.dbError);

			app.db.executeSql ("SELECT sku_name FROM sku WHERE sku_id = \"" + app.lastSKU + "\"", [], function (sqlResult) {
				if (sqlResult.rows.length == 1)
					$("#skuitemname").text (sqlResult.rows.item (0).sku_name);
			}, function () { alert ("c"); /* TODO What is this? */ });

			$("#cardrestockeditlast").text (recordStock.stock_qty);
			$("#cardrestockeditnow").text (recordStock.stock_qty);
			$("#cardrestockeditlast").data ("bias", "");

			$("#cardrestockeditsold").val (0);
			$("#cardrestockeditremoved").val (0);
			$("#cardrestockeditadded").val (0);
		} else {
			$("#cardrestockdo").addClass ("hide");
			$("#cardrestockedit").addClass ("hide");
			$("#cardrestockhome").removeClass ("hide");
		}
	},

	machineAddRecord: function () {
		app.machineEditRecord ({
			machine_id: "",
			machine_barcode: (app.lastScan && app.lastScan.length && app.lastScan.length == 6 && app.lastScan.substring (0, 3) == "441" ? app.lastScan : ""),
			machine_category_name: "",
			machine_name: "",
			machine_client: "",
			machine_location: "",
			machine_notes: ""
		});
	},

	machineDeleteRecord: function (rec) {
		navigator.notification.confirm ("This cannot be undone. Are you sure?", function (btnIndex) {
			if (btnIndex == 1) {
				app.db.executeSql ("UPDATE machine SET sync_point = " + app.getTime () + ", machine_deleted = 1 WHERE machine_id = \"" + rec.machine_id + "\"", [], app.dbNull, app.dbError);
				app.sectionShow ("machinelist");
			}
		}, "Delete Machine", ["Delete", "Cancel"]);
	},

	machineEditRecord: function (record) {
		$("#cardmachinelist").addClass ("hide");
		$("#cardmachineedit div").empty ();

		var options = "<option>Select&hellip;</option>";
		for (var i = 0; i < app.machineCategories.length; ++i)
			options += "<option>" + app.machineCategories[i].machine_category_name + "</option>";

		app.inEdition = "machine";
		$("#cardmachineedit div").append ("<form id=\"formmachineedit\" action=\"noscript.php\" method=\"post\">\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input required class=\"mdl-textfield__input\" type=\"text\" id=\"formmachineeditbarcode\" value=\"" + record.machine_barcode + "\">\n<label class=\"mdl-textfield__label\" for=\"formmachineeditbarcode\">Barcode</label>\n</div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\"><select class=\"mdl-textfield__input\" type=\"text\" id=\"formmachineedittype\">" + options + "</select></div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input required class=\"mdl-textfield__input\" type=\"text\" id=\"formmachineeditname\" value=\"" + record.machine_name + "\">\n<label class=\"mdl-textfield__label\" for=\"formmachineeditname\">Name</label>\n</div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input class=\"mdl-textfield__input\" type=\"text\" id=\"formmachineeditclient\" value=\"" + record.machine_client + "\">\n<label class=\"mdl-textfield__label\" for=\"formmachineeditclient\">Client</label>\n</div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input class=\"mdl-textfield__input\" type=\"text\" id=\"formmachineeditlocation\" value=\"" + record.machine_location + "\">\n<label class=\"mdl-textfield__label\" for=\"formmachineeditlocation\">Location</label>\n</div>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<textarea class=\"mdl-textfield__input\" id=\"formmachineeditnotes\" >" + record.machine_notes + "\n</textarea>\n<label class=\"mdl-textfield__label\" for=\"formmachineeditnotes\">Notes</label>\n</div><br>\n<br>\n<input required type=\"hidden\" id=\"formmachineeditid\" value=\"" + record.machine_id + "\"><br><button type=\"submit\" class=\"mdl-button mdl-js-button mdl-button--primary mdl-button--raised\">Save</button> <button type=\"button\" class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--accent cancelbtn\">Cancel</button>\n</form>\n");

		if (record.machine_id != "")
			$("#formmachineedittype").val (record.machine_category_name);

		$("#formmachineedit button.cancelbtn").on ("click", function (e) {
			e.preventDefault ();
			app.inEdition = "";
			app.sectionShow ("machinelist");
		});

		$("#formmachineedit").on ("submit", function (e) {
			e.preventDefault ();
			if ($("#formmachineedittype").prop ("selectedIndex") == 0 /* $("#formmachineedittype").val ().substring (0, 3) != "441"*/) { // TODO What is this?
				$("#formmachineedittype").css ({"border-color": "#ff0000"});
				return;
			}

			var machineUUID = app.genUUID ();
			var sqlCheck = "SELECT * FROM machine WHERE machine_barcode = \"" + $("#formmachineeditbarcode").val () + "\""; // FIXME Is SELECT * necessary, can't we use field names as it's more efficient?

			if (record.machine_id == "") {
				app.db.executeSql (sqlCheck, [], function (res) {
					if (res.rows.length > 0) {
						navigator.notification.alert ("This barcode is already assigned to a different machine", null, "Barcode conflict", "Ok");
					} else {
						var category_id = app.machineCategories[$("#formmachineedittype").prop ("selectedIndex") - 1].machine_category_id;
						var sqlOperation = "INSERT INTO machine (machine_id, sync_point, machine_barcode, machine_category_id, machine_name, machine_client, machine_location, machine_deleted, machine_notes) VALUES (\"" + machineUUID + "\", " + app.getTime () + ", \"" + $("#formmachineeditbarcode").val () + "\", \"" + category_id + "\", \"" + $("#formmachineeditname").val () + "\", \"" + $("#formmachineeditclient").val () + "\", \"" + $("#formmachineeditlocation").val () + "\", 0, \"" + $("#formmachineeditnotes").val () + "\")";
						app.db.executeSql (sqlOperation, [], function (sqlResult) {
							app.toastShow ("Machine saved");
							app.lastMachine = machineUUID;
							app.currentMachineName = $("#formmachineeditname").val ();
							app.sectionShow ("machinelist");
							app.inEdition = "";
						}, app.dbError);
					}
				}, app.dbError);
			} else {
				sqlCheck = sqlCheck + " AND machine_id != \"" + record.machine_id + "\"";

				app.db.executeSql (sqlCheck, [], function (res) {
					if (res.rows.length > 0) {
						navigator.notification.alert ("This barcode is already assigned to a different machine", null, "Barcode conflict", "Ok");
					} else {
						var new_category_id = app.machineCategories[$("#formmachineedittype").prop ("selectedIndex") - 1].machine_category_id;
						var updateSql = "UPDATE machine SET sync_point = " + app.getTime () + ", machine_barcode = \"" + $("#formmachineeditbarcode").val () + "\", machine_category_id = \"" + new_category_id + "\", machine_name = \"" + $("#formmachineeditname").val () + "\", machine_client = \"" + $("#formmachineeditclient").val () + "\", machine_location = \"" + $("#formmachineeditlocation").val () + "\", machine_notes = \"" + $("#formmachineeditnotes").val () + "\" WHERE machine_id = \"" + $("#formmachineeditid").val () + "\"";
						app.db.executeSql (updateSql, [], function (sqlResult) {
							app.toastShow ("Machine saved");
							app.sectionShow ("machinelist");
							app.inEdition = "";
						}, app.dbError);
					}
				}, app.dbError);
			}

			return false;
		});

		componentHandler.upgradeDom ();
		$("#cardmachineedit").removeClass ("hide");
	},

	machinelistSection: function (sectionInit, sectionAction) {
		if (sectionInit) {
			$("#cardmachineaddbtn").on ("click", function () {
				app.lastScan = "";
				app.inEdition = "machine";
				app.machineAddRecord ();
			});
		}

		if (sectionAction == "add") {
			app.machineAddRecord ();
		} else {
			$("#cardmachineedit").addClass ("hide");
			$("#cardmachinelist").removeClass ("hide");

			app.db.executeSql ("SELECT machine_id, machine.machine_category_id, machine_barcode, machine_name, machine_client, machine_location, machine_notes, machine_category.machine_category_id, machine_category_name FROM machine, machine_category WHERE machine.machine_category_id = machine_category.machine_category_id and machine_deleted = 0 ORDER BY machine_client ASC, machine_location ASC, machine_name ASC", [], function (sqlResult) {
				$("#cardmachinelist ul").empty ();
				for (var i = 0; i < sqlResult.rows.length; ++i) {
					var record = sqlResult.rows.item (i);
					var machine = $("<li class=\"mdl-list__item mdl-list__item--two-line is-active\">\n<span class=\"mdl-list__item-primary-content\"><i class=\"material-icons mdl-list__item-avatar\">gradient</i>" + (record.machine_client > "" ? record.machine_client + ", " : "") + record.machine_name + "\n<span class=\"mdl-list__item-text-body\">\n<br>\n<small><span class=\"mdl-color__lightgrey\">Machine type </span> " + record.machine_category_name + "</small><br>\n<br>\n<small><span class=\"mdl-color__lightgrey\">Location </span> " + record.machine_location + "</small><br>\n<small><span class=\"mdl-color__lightgrey\">Barcode </span> " + record.machine_barcode + "</small><br>\n<br>\n<button class=\"mdl-button mdl-js-button mdl-button--raised edit\" data-editmachine=\"" + record.machine_id + "\">Edit</button> <button class=\"mdl-button mdl-js-button mdl-button--raised delete\" data-editmachine=\"" + record.machine_id + "\">Delete</button></div></span>\n</span>\n<\/li>\n");

					$("button", machine)[0].record = record;
					$("button.edit", machine).on ("click", function () {
						app.inEdition = "machine";
						app.machineEditRecord (this.record);
						return false;
					});
					$("button.delete", machine)[0].record = record;
					$("button.delete", machine).on ("click", function () {
						app.machineDeleteRecord (this.record);
						return false;
					});
					machine.on ("click", function () {
						$(this).off ("click");
						$(this).removeClass ("is-active");
						$(this).animate ({"height": "200px"});
					});

					$("#cardmachinelist ul").append (machine);
				}
			}, app.dbError);
		}
	},

	skuEditRecord: function (record) {
		$("#cardskulist").addClass ("hide");
		$("#cardskuedit div").empty ();

		var con = "<option>Select&hellip;</option>";

		for (var item in app.skuCategories)
			con += "<option>" + app.skuCategories[item].sku_category_name + "</option>";

		$("#cardskuedit div").append ("<form id=\"formskuedit\" action=\"noscript.php\" method=\"post\">\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input required class=\"mdl-textfield__input\" type=\"text\" id=\"formskueditbarcode\" value=\"" + record.sku_barcode + "\">\n<label class=\"mdl-textfield__label\" for=\"formskueditbarcode\">Barcode</label>\n</div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\"><select required class=\"mdl-textfield__input\" type=\"text\" id=\"formskuedittype\" required value=\"" + record.sku_type + "\">" + con + "</select>\n</div>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<input required class=\"mdl-textfield__input\" type=\"text\" id=\"formskueditname\" value=\"" + record.sku_name + "\">\n<label class=\"mdl-textfield__label\" for=\"formskueditname\">Name</label>\n</div>\n<br>\n<br>\n<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label\">\n<textarea class=\"mdl-textfield__input\" id=\"formskueditnotes\" >" + record.sku_notes + "</textarea>\n<label class=\"mdl-textfield__label\" for=\"formskueditnotes\">Notes</label>\n</div><input required type=\"hidden\" id=\"formskueditid\" value=\"" + record.sku_id + "\"><br><button class=\"mdl-button mdl-js-button mdl-button--primary mdl-button--raised\">Save</button> <button type=\"button\" class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--accent cancelbtn\">Cancel</button>\n</form>\n")
		if (record.sku_category_name != "")
			$("#formskuedittype").val (record.sku_category_name);

		app.inEdition = "sku";
		$("#formskuedit").on ("submit", function (e) {
			e.preventDefault ();
			if ($("#formskuedittype").prop ("selectedIndex") == 0) {
				$("#formskuedittype").focus ().css ({"border-color": "#ff0000"});
				return;
			} else {
				$("#formskuedittype").css ({"border-color": "#3f51b5"});
			}
			var sqlCheck = "SELECT sku_id FROM sku WHERE sku_deleted = 0 AND sku_barcode = \"" + $("#formskueditbarcode").val () + "\"";
			if (record.sku_id == "") {
				app.db.executeSql (sqlCheck, [], function (res) {
					if (res.rows.length != 0) {
						navigator.notification.alert ("This barcode is already assigned to a different product");
					} else {
						var insertSql = "INSERT INTO sku (sku_id, sync_point, sku_barcode, sku_category_id, sku_name, sku_deleted, sku_notes) VALUES (\"" + app.genUUID () + "\", " + app.getTime () + ", \"" + $("#formskueditbarcode").val () + "\", \"" + app.skuCategories[$("#formskuedittype").prop ("selectedIndex") - 1].sku_category_id + "\", \"" + $("#formskueditname").val () + "\", 0, \"" + $("#formskueditnotes").val () + "\")";
						app.db.quickSql (insertSql);
						app.inEdition = "";
						app.sectionShow ("skulist");
					}
				}, app.dbError);
			} else {
				app.db.executeSql (sqlCheck + " AND sku_id != \"" + record.sku_id + "\"", [], function (res) {
					if (res.rows.length != 0) {
						navigator.notification.alert ("This barcode is already assigned to a different product");
					} else {
						var category_id = app.skuCategories[$("#formskuedittype").prop ("selectedIndex") - 1].sku_category_id;
						var updateSql = "UPDATE sku SET sync_point = " + app.getTime () + ", sku_barcode = \"" + $("#formskueditbarcode").val () + "\", sku_category_id = \"" + category_id + "\", sku_name = \"" + $("#formskueditname").val () + "\" , sku_notes = \"" + $("#formskueditnotes").val () + "\" WHERE sku_id = \"" + $("#formskueditid").val () + "\"";
						app.db.quickSql (updateSql);
						app.inEdition = "";
						app.sectionShow ("skulist");
					}
				}, app.dbError);
			}
			return false;
		});

		$("#formskuedit button.cancelbtn").on ("click", function (e) {
			app.sectionShow ("skulist");
			app.inEdition = "";
		});

		componentHandler.upgradeDom ();
		$("#cardskuedit").removeClass ("hide");
	},

	skuDeleteRecord: function (rec) {
		navigator.notification.confirm ("This cannot be undone. Are you sure?", function (btnIndex) {
			if (btnIndex == 1) {
				app.db.executeSql ("UPDATE sku SET sync_point = " + app.getTime () + ", sku_deleted = 1 WHERE sku_id = \"" + rec.sku_id + "\"", [], app.dbNull, app.dbError);
				app.sectionShow ("skulist");
			}
		}, "Delete SKU", ["Delete", "Cancel"]);
	},

	skuAddRecord: function () {
		app.skuEditRecord ({
			sku_id: "",
			sku_barcode: app.lastScan,
			sku_category_name: "",
			sku_name: "",
			sku_notes: ""
		});
	},

	filterSku: function  (category_index, query_match) {
		var sql = "SELECT sku.sku_category_id, sku_barcode, sku_notes, sku_name, sku_id, sku_category_name FROM sku, sku_category WHERE sku.sku_category_id = sku_category.sku_category_id AND sku_deleted = 0";
		if (category_index != 0)
			sql += " AND sku.sku_category_id = \"" + app.skuCategories[parseInt (category_index) - 1].sku_category_id + "\"";
		if (query_match)
			sql +=" AND (sku_name LIKE \"%" + query_match + "%\" OR sku_barcode LIKE \"%" + query_match + "%\")" ;
		sql += " ORDER BY sku_category_name ASC";
		app.db.executeSql (sql, [], function (sqlResult) {
			$("#cardskulist ul").empty ();

			for (var i = 0; i < sqlResult.rows.length; ++i) {
				var record = sqlResult.rows.item (i);
				var sku = $("<li class=\"mdl-list__item mdl-list__item--two-line is-active\">\n<span class=\"mdl-list__item-primary-content\"><i class=\"material-icons mdl-list__item-avatar\">shopping_basket</i>" + record.sku_name + "\n<span class=\"mdl-list__item-text-body\">\n<br>\n<small><span class=\"mdl-color__lightgrey\">SKU type </span> " + record.sku_category_name + "</small><br>\n<br>\n<small><span class=\"mdl-color__lightgrey\">Barcode </span> " + record.sku_barcode + "</small><br>\n<br>\n<button class=\"mdl-button mdl-js-button mdl-button--raised editbtn\">Edit</button> <button class=\"mdl-button mdl-js-button mdl-button--raised deletebtn\">Delete</button> <button class=\"mdl-button mdl-js-button mdl-button--raised stockbtn\">Stock</button></div></span>\n</span>\n</li>\n");
				$("button", sku)[0].record = record;
				$("button.editbtn", sku).on ("click", function () {
					app.inEdition = this.record;
					app.skuEditRecord (this.record);
					return false;
				});
				$("button.deletebtn", sku)[0].record = record;
				$("button.deletebtn", sku).on ("click", function () {
					app.skuDeleteRecord (this.record);
					return false;
				});

				$("button.stockbtn", sku)[0].record = record;
				$("button.stockbtn", sku).on ("click", function () {
					app.lastSKU = this.record.sku_id;
					if (app.lastMachine) {
						//restock selected machine with this
						app.sectionShow ("restock", "edit");
					} else {
						//if no machine has been selected yet to restock
						app.sectionShow ("restock");
					}
					return false;
				});
				sku.on ("click", function () {
					$(this).off ("click");
					$(this).removeClass ("is-active");
					$(this).animate ({"height": "200px"});
				});
				$("#cardskulist ul").append (sku);
			}
		}, app.dbError);
	},

	skulistSection: function (sectionInit, sectionAction) {
		if (sectionInit) {
			$("#cardskuaddbtn").on ("click", function () {
				app.lastScan = "";
				app.inEdition = "sku";
				app.skuAddRecord ();
			});

			$("#skufilter").on ("change", function () {
				app.filters.skuFilterIndex = $(this).prop ("selectedIndex");
				app.filterSku ($(this).prop ("selectedIndex"), $("#sku_search_query").val ());
			});

			$("#sku_search_query").on ("keydown",function () {
				app.filterSku ($("#skufilter").prop ("selectedIndex"), $(this).val ());
			});
		}

		if (sectionAction == "add") {
			app.skuAddRecord ();
		} else {
			$("#cardskuedit").addClass ("hide");
			$("#cardskulist").removeClass ("hide");
			$("#skufilter").empty ().append ("<option>(All)</option>");

			for (var i = 0; i < app.skuCategories.length; ++i)
				$("#skufilter").append ("<option>" + app.skuCategories[i].sku_category_name + "</option>");

				
			$("#skufilter").prop ("selectedIndex", app.filters.skuFilterIndex).change ();
		}
	},

	syncSection: function (sectionInit, sectionAction) {
		$("#sectionsync").removeClass ("hide");

		if (sectionInit) {
			$("#sectionsync button.syncbtn").on ("click", function (e) {
				if (navigator.connection.type != "none") {
					app.toastShow ("Synchronising, please wait…");
					$("#sectionsync button.syncbtn").attr ("disabled", "true");
					app.syncDo (0);
				} else {
					app.toastShow ("Get connected and press sync again");
				}
			});
		}

		if (navigator.connection.type != "none") {
			app.toastShow ("Sync when all barcodes entered");
		} else {
			app.toastShow ("Get connected and press sync again");
		}
	},

	syncDo: function (table) {
		app.db.executeSql ("SELECT * FROM " + app.tables[table] + (app.tables[table] != "restock" ? " WHERE sync_point > " + app.syncPoint : "") + " ORDER BY _ROWID_ ASC", [], function (res) {
			var data = [];
			if (app.tables[table] != "stock"){
				for (var i = 0; i < res.rows.length; ++i)
					data.push (res.rows.item (i));	
			}

			app.ajax ("data", "1", "sync." + app.tables[table], {data: data}, function (response, textStatus, jqXHR) {
				if (jqXHR.success) {
					if (!response.success)
						return app.syncFinish (false);

					for (var i = 0; i < response.data.length; ++i) {
						(function (table, responserow) {
							var keys = Object.keys (responserow);
							app.db.executeSql ("SELECT " + table + "_id FROM " + table + " WHERE " + table + "_id = \"" + response.data[i][table + "_id"] + "\"", [], function (result) {
								var sqlOperation = "";

								if (result.rows.length == 0) {
									sqlOperation = "INSERT INTO " + table + " (";
									for (var j = 0; j < keys.length; ++j)
										sqlOperation += keys[j] + ", ";
									sqlOperation = sqlOperation.substring (0, sqlOperation.length - 2);
									sqlOperation += ") VALUES (";
									for (var j = 0; j < keys.length; ++j) {
										var isNumber = (typeof responserow[keys[j]] != "string");
										sqlOperation += (isNumber ? "" : "\"") + responserow[keys[j]] + (isNumber ? "" : "\"") + ", ";
									}
									sqlOperation = sqlOperation.substring (0, sqlOperation.length - 2);
									sqlOperation += ")";
								} else {
									sqlOperation = "UPDATE " + table + " SET ";
									for (var j = 0; j < keys.length; ++j) {
										var isNumber = (typeof responserow[keys[j]] != "string");
										sqlOperation += keys[j] + " = " + (isNumber ? "" : "\"") + responserow[keys[j]] + (isNumber ? "" : "\"") + ", ";
									}
									sqlOperation = sqlOperation.substring (0, sqlOperation.length - 2);
									sqlOperation += " WHERE " + table + "_id = \"" + responserow[table + "_id"] + "\"";
								}

								app.db.quickSql (sqlOperation);
							});
						}) (app.tables[table], response.data[i]);
					}

					if (app.tables[table] == "restock")
						app.db.executeSql ("DELETE FROM restock", [], app.dbNull, app.dbError);

					if (table < app.tables.length - 1) {
						app.syncDo (++table);
					} else {
						app.syncFinish (true);
					}

					return;
				}

				app.syncFinish (false);
			});
		}, app.dbError);
	},

	syncFinish: function (success) {
		$("#sectionsync button.syncbtn").removeAttr ("disabled");

		if (success) {
			app.syncPoint = app.getTime();
			app.settingSave ("sync_point", app.syncPoint);

			app.toastShow ("Data successfully synchronised");
		} else {
			app.toastShow ("Data synchronisation failed, try again");
		}

		app.dataLoad ();
		app.db.quickSql ("VACUUM");
	}
};

app.initialise ();
